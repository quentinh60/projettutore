﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleLib
{
    public class Tuile
    {
        private string couleur;
        private string symbole;

        public Tuile(string couleur,string symbole)
        {
            this.symbole = symbole;
            this.couleur = couleur;
        }

        public string getColor()
        {
            return couleur;
        }
        
        public string getSymbol()
        {
            return symbole;
        }
    }
}
