﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QwirkleLib
{
    public class Joueur
    {

        private string nom;
        private int score;
        private bool tour;
        private int NoJoueur;
        private int plusGrandeCombinaison;
        public Joueur(string nom, int score, bool tour, int NoJoueur, int plusGrdCombi)
        { 
            this.nom = nom;
            this.score = score;
            this.tour = tour;
            this.NoJoueur = NoJoueur;
            this.plusGrandeCombinaison = plusGrdCombi;
        }

        public int GetNoJoueur()
        {
            return this.NoJoueur;
        }
        public bool GetTour()
        {
            return this.tour;
        }

        public void SetTour(bool tour)
        {
            this.tour = tour;
        }

        public string GetNom()
        {
            return this.nom;
        }
        
        public int GetScore()
        {
            return this.score;
        }

        public void SetScore(int score)
        {
            this.score = score;
        }

        public int PlusGrandeCombinaison()
        {
            return plusGrandeCombinaison;
        }
    }
}
