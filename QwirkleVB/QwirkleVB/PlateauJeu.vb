﻿Imports QwirkleLib
Imports System.IO


Public Class PlateauJeu

    Public lesJoueurs As New List(Of Joueur)
    Public pioche As New List(Of Tuile)
    Public Main1 As New List(Of Tuile)
    Public Main2 As New List(Of Tuile)
    Public Main3 As New List(Of Tuile)
    Public Main4 As New List(Of Tuile)
    Public Couleurs As List(Of String) = New List(Of String)({"Bleu", "Rouge", "Violet", "Orange", "Vert", "Jaune"})
    Public Formes As List(Of String) = New List(Of String)({"Rond", "Etoile", "Carre", "Trefle", "Croix", "Losange"})

    Dim Plateau As New Panel
    Private Sub pic_MouseMove(sender As Object, e As MouseEventArgs) Handles picmain1.MouseMove, picmain2.MouseMove, picmain3.MouseMove, picmain4.MouseMove, picmain5.MouseMove, picmain6.MouseMove, picechange6.MouseMove, picechange5.MouseMove, picechange4.MouseMove, picechange3.MouseMove, picechange2.MouseMove, picechange1.MouseMove

        Dim effetRealise As DragDropEffects
        Dim pic As PictureBox = sender
        If e.Button = MouseButtons.Left AndAlso pic.Image IsNot Nothing Then
            pic.AllowDrop = False
            effetRealise = pic.DoDragDrop(pic.Image, DragDropEffects.Copy)
            If effetRealise = DragDropEffects.Copy Then
                pic.Image = Nothing
            End If
            pic.AllowDrop = True
        End If
        If pic.Image IsNot Nothing Then
            pic.AllowDrop = False
        End If
    End Sub

    Private Sub pic_DragEnter(sender As Object, e As DragEventArgs) Handles picmain1.DragEnter, picmain2.DragEnter, picmain3.DragEnter, picmain4.DragEnter, picmain5.DragEnter, picmain6.DragEnter, picechange6.DragEnter, picechange5.DragEnter, picechange4.DragEnter, picechange3.DragEnter, picechange2.DragEnter, picechange1.DragEnter
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub pic_DragDrop(sender As Object, e As DragEventArgs) Handles picmain1.DragDrop, picmain2.DragDrop, picmain3.DragDrop, picmain4.DragDrop, picmain5.DragDrop, picmain6.DragDrop, picechange6.DragDrop, picechange5.DragDrop, picechange4.DragDrop, picechange3.DragDrop, picechange2.DragDrop, picechange1.DragDrop
        Dim pic As PictureBox = sender
        pic.Image = e.Data.GetData(DataFormats.Bitmap)
    End Sub

    Private Sub PlateauJeuLoad(sender As Object, e As EventArgs) Handles MyBase.Load
        'Création des controles (picturebox) dynamique'
        Dim l As Byte
        Dim c As Byte

        Plateau.Size = New Size(542, 542)
        Plateau.Location = New Point(350, 80)
        Plateau.BorderStyle = BorderStyle.FixedSingle
        Plateau.AutoScroll = True
        Me.Controls.Add(Plateau)
        For l = 1 To 9
            For c = 1 To 9
                Dim newpic As New PictureBox
                Dim dernierpicCree As PictureBox
                newpic.Name = "pic_" & l & "_" & c
                newpic.Size = New Size(60, 60)
                newpic.BorderStyle = BorderStyle.FixedSingle
                newpic.SizeMode = PictureBoxSizeMode.StretchImage
                If l = 1 And c = 1 Then
                    newpic.Location = New Point(0, 0)
                Else
                    If l > 1 And c = 1 Then
                        dernierpicCree = Plateau.Controls("pic_" & (l - 1) & "_" & c)
                        newpic.Location = New Point(dernierpicCree.Location.X, dernierpicCree.Location.Y + 60)
                    Else
                        dernierpicCree = Plateau.Controls("pic_" & l & "_" & (c - 1))
                        newpic.Location = New Point(dernierpicCree.Location.X + 60, dernierpicCree.Location.Y)
                    End If
                End If

                Plateau.Controls.Add(newpic)
                newpic.AllowDrop = True
                AddHandler newpic.DragDrop, AddressOf pic_DragDrop
                AddHandler newpic.DragEnter, AddressOf pic_DragEnter
                AddHandler newpic.MouseMove, AddressOf pic_MouseMove
            Next
        Next

        Dim j As Byte
        Dim picechange As PictureBox
        For j = 1 To 6
            picechange = Me.PnlEchange.Controls("picechange" & j)
            picechange.AllowDrop = True
            AddHandler picechange.DragEnter, AddressOf pic_DragEnter
            AddHandler picechange.DragDrop, AddressOf pic_DragDrop
        Next

        'Pioche'
        For NoColor As Byte = 0 To 5
            For NoSymbol As Byte = 0 To 5
                For Nbpiece As Byte = 1 To 3
                    Dim Tuile As New Tuile(Couleurs(NoColor), Formes(NoSymbol))
                    pioche.Add(Tuile)
                Next
            Next
        Next

        'Main(tous les joueurs)'
        Dim PicMain As PictureBox
        Dim pos As New Integer
        Dim rdm As New Random
        For i As Byte = 0 To 5
            pos = rdm.Next(0, pioche.Count)
            Main1.Add(pioche(pos))
            pioche.RemoveAt(pos)
            pos = rdm.Next(0, pioche.Count)
            Main2.Add(pioche(pos))
            pioche.RemoveAt(pos)
            If lesJoueurs.Count >= 3 Then
                pos = rdm.Next(0, pioche.Count)
                Main3.Add(pioche(pos))
                pioche.RemoveAt(pos)
            End If
            If lesJoueurs.Count = 4 Then
                pos = rdm.Next(0, pioche.Count)
                Main4.Add(pioche(pos))
                pioche.RemoveAt(pos)
            End If
        Next
        For nbpic As Byte = 0 To 5
            PicMain = Me.Controls("PicMain" & nbpic + 1)
            PicMain.Image = Image.FromFile(Path.GetDirectoryName(Path.GetDirectoryName(Application.StartupPath)) & "\Resources\" & Main1(nbpic).getSymbol & Main1(nbpic).getColor & ".jpg")
        Next

        MsgBox("Tour du Joueur " & lesJoueurs(0).GetNoJoueur)
        TimerTour.Interval = 1000
        TimerTour.Start()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As System.EventArgs) Handles TimerTour.Tick
        PBTour.Value += 1
        If PBTour.Value = 60 Then
            TimerTour.Stop()
            PBTour.Value = 0
            'Changement de tour automatique lors de la fin du timer'
            BoutonValiderMouv.PerformClick()
        End If
    End Sub

    Private Sub BoutonQuitter_Click(sender As Object, e As EventArgs) Handles BoutonQuitter.Click
        TimerTour.Stop()
        frmFinDePartie.Show()
        Me.Hide()
    End Sub

    Private Sub BoutonValiderMouv_Click(sender As Object, e As EventArgs) Handles BoutonValiderMouv.Click
        'Passage des tours'
        Dim numero As Integer
        Dim nbjoueur As Integer = 0
        Dim PicMain As PictureBox

        For Each joueur As Joueur In lesJoueurs
            nbjoueur = nbjoueur + 1
        Next

        For Each pic As PictureBox In PnlEchange.Controls

            If pic.Image IsNot Nothing Then
                For NoColor As Byte = 0 To 5
                    For NoSymbol As Byte = 0 To 5
                        Dim tuile As New Tuile(Couleurs(NoColor), Formes(NoSymbol))
                        If pic.Image.Tag = tuile.getColor() & tuile.getSymbol Then
                            pioche.Add(tuile)
                        End If
                    Next
                Next
                pic.Image = Nothing
                pic.AllowDrop = True
            End If
        Next


        For Each joueur As Joueur In lesJoueurs
            If joueur.GetTour = True Then
                joueur.SetTour(False)
                If joueur.GetNoJoueur = nbjoueur Then
                    numero = 0
                Else
                    numero = joueur.GetNoJoueur
                End If
            End If
        Next

        'Fixe la tuile dans la picturebox si le mouvement est validé'
        Dim point As Integer = 0
        For Each c As PictureBox In Me.Plateau.Controls

            If c.Image IsNot Nothing Then

                point = point + 1
                If numero = 0 Then
                    Me.Controls("lbl" & nbjoueur).Text = "Points Joueur " & nbjoueur & ": " & point
                Else
                    Me.Controls("lbl" & numero).Text = "Points Joueur " & numero & ": " & point
                End If

                c.AllowDrop = False
                RemoveHandler c.DragDrop, AddressOf pic_DragDrop
                RemoveHandler c.DragEnter, AddressOf pic_DragEnter
                RemoveHandler c.MouseMove, AddressOf pic_MouseMove
            End If
        Next

        'Suppression des tuile posée'
        For nbpic As Integer = 5 To 0 Step -1
            PicMain = Me.Controls("PicMain" & nbpic + 1)
            If PicMain.Image Is Nothing Then
                If numero = 1 Then
                    Main1.RemoveAt(nbpic)
                End If
                If numero = 2 Then
                    Main2.RemoveAt(nbpic)
                End If
                If numero = 3 Then
                    Main3.RemoveAt(nbpic)
                End If
                If numero = 4 Then
                    Main4.RemoveAt(nbpic)
                End If
            End If
        Next

        'Retirage des tuiles dans la main du joueur et suppression dans la pioche'
        Dim rdm As New Random
        Dim pos As New Integer
        For nbpic As Integer = 0 To 5
            PicMain = Me.Controls("PicMain" & nbpic + 1)

            If PicMain.Image Is Nothing Then
                If numero = 1 And Main1.Count < 6 Then
                    pos = rdm.Next(0, pioche.Count)
                    Main1.Add(pioche(pos))
                    pioche.RemoveAt(pos)
                End If
                If numero = 2 And Main2.Count < 6 Then
                    pos = rdm.Next(0, pioche.Count)
                    Main2.Add(pioche(pos))
                    pioche.RemoveAt(pos)
                End If
                If numero = 3 And Main3.Count < 6 Then
                    pos = rdm.Next(0, pioche.Count)
                    Main3.Add(pioche(pos))
                    pioche.RemoveAt(pos)
                End If
                If numero = 4 And Main4.Count < 6 Then
                    pos = rdm.Next(0, pioche.Count)
                    Main4.Add(pioche(pos))
                    pioche.RemoveAt(pos)
                End If
            End If
        Next
        'Attribution d'un tour'
        For Each joueur As Joueur In lesJoueurs
            If joueur.GetNoJoueur = numero + 1 Then
                joueur.SetTour(True)
            End If
        Next

        'Affichage de la main du joueur en cours'
        For Each joueur As Joueur In lesJoueurs

            If joueur.GetTour = True And joueur.GetNoJoueur = 1 Then
                For nbpic As Byte = 0 To 5
                    PicMain = Me.Controls("PicMain" & nbpic + 1)
                    PicMain.Image = Image.FromFile(Path.GetDirectoryName(Path.GetDirectoryName(Application.StartupPath)) & "\Resources\" & Main1(nbpic).getSymbol & Main1(nbpic).getColor & ".jpg")
                    PicMain.Image.Tag = Main1(nbpic).getColor & Main1(nbpic).getSymbol
                Next
            End If
            If joueur.GetTour = True And joueur.GetNoJoueur = 2 Then
                For nbpic As Byte = 0 To 5
                    PicMain = Me.Controls("PicMain" & nbpic + 1)
                    PicMain.Image = Image.FromFile(Path.GetDirectoryName(Path.GetDirectoryName(Application.StartupPath)) & "\Resources\" & Main2(nbpic).getSymbol & Main2(nbpic).getColor & ".jpg")
                    PicMain.Image.Tag = Main2(nbpic).getColor & Main2(nbpic).getSymbol
                Next
            End If
            If joueur.GetTour = True And joueur.GetNoJoueur = 3 Then
                For nbpic As Byte = 0 To 5
                    PicMain = Me.Controls("PicMain" & nbpic + 1)
                    PicMain.Image = Image.FromFile(Path.GetDirectoryName(Path.GetDirectoryName(Application.StartupPath)) & "\Resources\" & Main3(nbpic).getSymbol & Main3(nbpic).getColor & ".jpg")
                    PicMain.Image.Tag = Main3(nbpic).getColor & Main3(nbpic).getSymbol
                Next
            End If
            If joueur.GetTour = True And joueur.GetNoJoueur = 4 Then
                For nbpic As Byte = 0 To 5
                    PicMain = Me.Controls("PicMain" & nbpic + 1)
                    PicMain.Image = Image.FromFile(Path.GetDirectoryName(Path.GetDirectoryName(Application.StartupPath)) & "\Resources\" & Main4(nbpic).getSymbol & Main4(nbpic).getColor & ".jpg")
                    PicMain.Image.Tag = Main4(nbpic).getColor & Main4(nbpic).getSymbol
                Next
            End If
        Next

        'échange de tuiles (Ton code)'
        For Each pic As PictureBox In Me.PnlEchange.Controls
            If pic.Image IsNot Nothing Then
                For NoColor As Byte = 0 To 5
                    For NoSymbol As Byte = 0 To 5
                        Dim tuile As New Tuile(Couleurs(NoColor), Formes(NoSymbol))
                        If pic.Image.Tag = tuile.getColor() & tuile.getSymbol() Then
                            pioche.Add(tuile)
                        End If
                    Next
                Next
                pic.Image = Nothing
                pic.AllowDrop = True
            End If
        Next

        MsgBox("Tour du Joueur " & numero + 1)
        'Reset du timer changement de tour'
        TimerTour.Stop()
        PBTour.Value = 0
        TimerTour.Start()

    End Sub

End Class
