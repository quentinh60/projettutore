﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Accueil
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Accueil))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ChoixNbJ = New System.Windows.Forms.Label()
        Me.J2 = New System.Windows.Forms.RadioButton()
        Me.J3 = New System.Windows.Forms.RadioButton()
        Me.J4 = New System.Windows.Forms.RadioButton()
        Me.PnlNbJ = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.JouezAvecVosAmis = New System.Windows.Forms.Label()
        Me.Jouer = New System.Windows.Forms.Button()
        Me.Quitter = New System.Windows.Forms.Button()
        Me.Règles = New System.Windows.Forms.Button()
        Me.Options = New System.Windows.Forms.Button()
        Me.WindowsMediaPlayer1 = New AxWMPLib.AxWindowsMediaPlayer()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlNbJ.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.WindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(157, 99)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(644, 171)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 4
        Me.PictureBox1.TabStop = False
        '
        'ChoixNbJ
        '
        Me.ChoixNbJ.Font = New System.Drawing.Font("Comic Sans MS", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChoixNbJ.Location = New System.Drawing.Point(59, 0)
        Me.ChoixNbJ.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.ChoixNbJ.Name = "ChoixNbJ"
        Me.ChoixNbJ.Size = New System.Drawing.Size(665, 65)
        Me.ChoixNbJ.TabIndex = 2
        Me.ChoixNbJ.Text = " Nombre de joueurs :"
        Me.ChoixNbJ.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'J2
        '
        Me.J2.Appearance = System.Windows.Forms.Appearance.Button
        Me.J2.AutoSize = True
        Me.J2.Font = New System.Drawing.Font("Comic Sans MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.J2.Location = New System.Drawing.Point(145, 81)
        Me.J2.Margin = New System.Windows.Forms.Padding(4)
        Me.J2.Name = "J2"
        Me.J2.Size = New System.Drawing.Size(136, 43)
        Me.J2.TabIndex = 3
        Me.J2.TabStop = True
        Me.J2.Text = "2 Joueurs"
        Me.J2.UseVisualStyleBackColor = True
        '
        'J3
        '
        Me.J3.Appearance = System.Windows.Forms.Appearance.Button
        Me.J3.AutoSize = True
        Me.J3.Font = New System.Drawing.Font("Comic Sans MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.J3.Location = New System.Drawing.Point(328, 81)
        Me.J3.Margin = New System.Windows.Forms.Padding(4)
        Me.J3.Name = "J3"
        Me.J3.Size = New System.Drawing.Size(136, 43)
        Me.J3.TabIndex = 4
        Me.J3.TabStop = True
        Me.J3.Text = "3 Joueurs"
        Me.J3.UseVisualStyleBackColor = True
        '
        'J4
        '
        Me.J4.Appearance = System.Windows.Forms.Appearance.Button
        Me.J4.AutoSize = True
        Me.J4.Font = New System.Drawing.Font("Comic Sans MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.J4.Location = New System.Drawing.Point(502, 81)
        Me.J4.Margin = New System.Windows.Forms.Padding(4)
        Me.J4.Name = "J4"
        Me.J4.Size = New System.Drawing.Size(136, 43)
        Me.J4.TabIndex = 5
        Me.J4.TabStop = True
        Me.J4.Text = "4 Joueurs"
        Me.J4.UseVisualStyleBackColor = True
        '
        'PnlNbJ
        '
        Me.PnlNbJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PnlNbJ.Controls.Add(Me.J4)
        Me.PnlNbJ.Controls.Add(Me.J3)
        Me.PnlNbJ.Controls.Add(Me.J2)
        Me.PnlNbJ.Controls.Add(Me.ChoixNbJ)
        Me.PnlNbJ.Location = New System.Drawing.Point(568, 418)
        Me.PnlNbJ.Margin = New System.Windows.Forms.Padding(4)
        Me.PnlNbJ.Name = "PnlNbJ"
        Me.PnlNbJ.Size = New System.Drawing.Size(784, 157)
        Me.PnlNbJ.TabIndex = 5
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.JouezAvecVosAmis)
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Location = New System.Drawing.Point(481, 12)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(958, 331)
        Me.Panel2.TabIndex = 7
        '
        'JouezAvecVosAmis
        '
        Me.JouezAvecVosAmis.Font = New System.Drawing.Font("Comic Sans MS", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.JouezAvecVosAmis.Location = New System.Drawing.Point(109, 29)
        Me.JouezAvecVosAmis.Name = "JouezAvecVosAmis"
        Me.JouezAvecVosAmis.Size = New System.Drawing.Size(741, 38)
        Me.JouezAvecVosAmis.TabIndex = 5
        Me.JouezAvecVosAmis.Text = "Jouez au Qwirkle avec vos amis !"
        Me.JouezAvecVosAmis.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Jouer
        '
        Me.Jouer.Font = New System.Drawing.Font("Comic Sans MS", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Jouer.Location = New System.Drawing.Point(810, 671)
        Me.Jouer.Name = "Jouer"
        Me.Jouer.Size = New System.Drawing.Size(300, 60)
        Me.Jouer.TabIndex = 8
        Me.Jouer.Text = "Jouer une partie"
        Me.Jouer.UseVisualStyleBackColor = True
        '
        'Quitter
        '
        Me.Quitter.Font = New System.Drawing.Font("Comic Sans MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Quitter.Location = New System.Drawing.Point(860, 811)
        Me.Quitter.Name = "Quitter"
        Me.Quitter.Size = New System.Drawing.Size(200, 55)
        Me.Quitter.TabIndex = 9
        Me.Quitter.Text = "Quitter le Jeu"
        Me.Quitter.UseVisualStyleBackColor = True
        '
        'Règles
        '
        Me.Règles.Font = New System.Drawing.Font("Comic Sans MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Règles.Location = New System.Drawing.Point(757, 746)
        Me.Règles.Name = "Règles"
        Me.Règles.Size = New System.Drawing.Size(200, 50)
        Me.Règles.TabIndex = 10
        Me.Règles.Text = "Règles"
        Me.Règles.UseVisualStyleBackColor = True
        '
        'Options
        '
        Me.Options.Font = New System.Drawing.Font("Comic Sans MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Options.Location = New System.Drawing.Point(963, 746)
        Me.Options.Name = "Options"
        Me.Options.Size = New System.Drawing.Size(200, 50)
        Me.Options.TabIndex = 11
        Me.Options.Text = "Options"
        Me.Options.UseVisualStyleBackColor = True
        '
        'WindowsMediaPlayer1
        '
        Me.WindowsMediaPlayer1.Enabled = True
        Me.WindowsMediaPlayer1.Location = New System.Drawing.Point(1662, 737)
        Me.WindowsMediaPlayer1.Name = "WindowsMediaPlayer1"
        Me.WindowsMediaPlayer1.OcxState = CType(resources.GetObject("WindowsMediaPlayer1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.WindowsMediaPlayer1.Size = New System.Drawing.Size(112, 61)
        Me.WindowsMediaPlayer1.TabIndex = 12
        '
        'Accueil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1920, 1080)
        Me.Controls.Add(Me.WindowsMediaPlayer1)
        Me.Controls.Add(Me.Options)
        Me.Controls.Add(Me.Règles)
        Me.Controls.Add(Me.Quitter)
        Me.Controls.Add(Me.Jouer)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.PnlNbJ)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "Accueil"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Accueil"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlNbJ.ResumeLayout(False)
        Me.PnlNbJ.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.WindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ChoixNbJ As Label
    Friend WithEvents J2 As RadioButton
    Friend WithEvents J3 As RadioButton
    Friend WithEvents J4 As RadioButton
    Friend WithEvents PnlNbJ As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents JouezAvecVosAmis As Label
    Friend WithEvents Jouer As Button
    Friend WithEvents Quitter As Button
    Friend WithEvents Règles As Button
    Friend WithEvents Options As Button
    Friend WithEvents WindowsMediaPlayer1 As AxWMPLib.AxWindowsMediaPlayer
End Class
