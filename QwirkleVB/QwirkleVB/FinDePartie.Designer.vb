﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmFinDePartie
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFinDePartie))
        Me.cmdRejouer = New System.Windows.Forms.Button()
        Me.RetourMenu = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GrpScore = New System.Windows.Forms.GroupBox()
        Me.LblJ1 = New System.Windows.Forms.Label()
        Me.LblJ2 = New System.Windows.Forms.Label()
        Me.LblJ3 = New System.Windows.Forms.Label()
        Me.LblJ4 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpScore.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdRejouer
        '
        Me.cmdRejouer.Font = New System.Drawing.Font("Comic Sans MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdRejouer.Location = New System.Drawing.Point(53, 474)
        Me.cmdRejouer.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmdRejouer.Name = "cmdRejouer"
        Me.cmdRejouer.Size = New System.Drawing.Size(255, 43)
        Me.cmdRejouer.TabIndex = 0
        Me.cmdRejouer.Text = "Reprendre la Partie"
        Me.cmdRejouer.UseVisualStyleBackColor = True
        '
        'RetourMenu
        '
        Me.RetourMenu.Font = New System.Drawing.Font("Comic Sans MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RetourMenu.Location = New System.Drawing.Point(544, 474)
        Me.RetourMenu.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.RetourMenu.Name = "RetourMenu"
        Me.RetourMenu.Size = New System.Drawing.Size(273, 43)
        Me.RetourMenu.TabIndex = 1
        Me.RetourMenu.Text = "Retour Menu Principal"
        Me.RetourMenu.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(207, 30)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(451, 107)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'GrpScore
        '
        Me.GrpScore.Controls.Add(Me.LblJ4)
        Me.GrpScore.Controls.Add(Me.LblJ3)
        Me.GrpScore.Controls.Add(Me.LblJ2)
        Me.GrpScore.Controls.Add(Me.LblJ1)
        Me.GrpScore.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.GrpScore.Location = New System.Drawing.Point(53, 188)
        Me.GrpScore.Name = "GrpScore"
        Me.GrpScore.Size = New System.Drawing.Size(764, 248)
        Me.GrpScore.TabIndex = 3
        Me.GrpScore.TabStop = False
        Me.GrpScore.Text = "Scoreboard"
        '
        'LblJ1
        '
        Me.LblJ1.AutoSize = True
        Me.LblJ1.Font = New System.Drawing.Font("Comic Sans MS", 13.8!)
        Me.LblJ1.Location = New System.Drawing.Point(65, 44)
        Me.LblJ1.Name = "LblJ1"
        Me.LblJ1.Size = New System.Drawing.Size(190, 33)
        Me.LblJ1.TabIndex = 0
        Me.LblJ1.Text = "Score Joueur 1:"
        '
        'LblJ2
        '
        Me.LblJ2.AutoSize = True
        Me.LblJ2.Font = New System.Drawing.Font("Comic Sans MS", 13.8!)
        Me.LblJ2.Location = New System.Drawing.Point(65, 94)
        Me.LblJ2.Name = "LblJ2"
        Me.LblJ2.Size = New System.Drawing.Size(194, 33)
        Me.LblJ2.TabIndex = 1
        Me.LblJ2.Text = "Score Joueur 2:"
        '
        'LblJ3
        '
        Me.LblJ3.AutoSize = True
        Me.LblJ3.Font = New System.Drawing.Font("Comic Sans MS", 13.8!)
        Me.LblJ3.Location = New System.Drawing.Point(65, 144)
        Me.LblJ3.Name = "LblJ3"
        Me.LblJ3.Size = New System.Drawing.Size(194, 33)
        Me.LblJ3.TabIndex = 2
        Me.LblJ3.Text = "Score Joueur 3:"
        '
        'LblJ4
        '
        Me.LblJ4.AutoSize = True
        Me.LblJ4.Font = New System.Drawing.Font("Comic Sans MS", 13.8!)
        Me.LblJ4.Location = New System.Drawing.Point(65, 194)
        Me.LblJ4.Name = "LblJ4"
        Me.LblJ4.Size = New System.Drawing.Size(194, 33)
        Me.LblJ4.TabIndex = 3
        Me.LblJ4.Text = "Score Joueur 4:"
        '
        'frmFinDePartie
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(865, 566)
        Me.Controls.Add(Me.GrpScore)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.RetourMenu)
        Me.Controls.Add(Me.cmdRejouer)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "frmFinDePartie"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FinDePartie"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpScore.ResumeLayout(False)
        Me.GrpScore.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cmdRejouer As Button
    Friend WithEvents RetourMenu As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents GrpScore As GroupBox
    Friend WithEvents LblJ4 As Label
    Friend WithEvents LblJ3 As Label
    Friend WithEvents LblJ2 As Label
    Friend WithEvents LblJ1 As Label
End Class
