﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MenuOptions
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MenuOptions))
        Me.Volume = New System.Windows.Forms.TrackBar()
        Me.LabelVolume = New System.Windows.Forms.Label()
        Me.value0 = New System.Windows.Forms.Label()
        Me.Value5 = New System.Windows.Forms.Label()
        Me.Value10 = New System.Windows.Forms.Label()
        Me.imgvolume = New System.Windows.Forms.PictureBox()
        Me.imgQwirkle = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Affichage = New System.Windows.Forms.ComboBox()
        Me.AxWindowsMediaPlayer1 = New AxWMPLib.AxWindowsMediaPlayer()
        Me.ValidParametre = New System.Windows.Forms.Button()
        CType(Me.Volume, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgvolume, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgQwirkle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Volume
        '
        Me.Volume.LargeChange = 1
        Me.Volume.Location = New System.Drawing.Point(719, 488)
        Me.Volume.Maximum = 6
        Me.Volume.Name = "Volume"
        Me.Volume.Size = New System.Drawing.Size(564, 56)
        Me.Volume.TabIndex = 0
        '
        'LabelVolume
        '
        Me.LabelVolume.AutoSize = True
        Me.LabelVolume.Font = New System.Drawing.Font("Comic Sans MS", 19.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelVolume.Location = New System.Drawing.Point(844, 384)
        Me.LabelVolume.Name = "LabelVolume"
        Me.LabelVolume.Size = New System.Drawing.Size(252, 46)
        Me.LabelVolume.TabIndex = 1
        Me.LabelVolume.Text = "Volume du jeu :"
        '
        'value0
        '
        Me.value0.AutoSize = True
        Me.value0.Font = New System.Drawing.Font("Comic Sans MS", 19.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.value0.Location = New System.Drawing.Point(711, 523)
        Me.value0.Name = "value0"
        Me.value0.Size = New System.Drawing.Size(40, 46)
        Me.value0.TabIndex = 3
        Me.value0.Text = "0"
        '
        'Value5
        '
        Me.Value5.AutoSize = True
        Me.Value5.Font = New System.Drawing.Font("Comic Sans MS", 19.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Value5.Location = New System.Drawing.Point(981, 523)
        Me.Value5.Name = "Value5"
        Me.Value5.Size = New System.Drawing.Size(40, 46)
        Me.Value5.TabIndex = 4
        Me.Value5.Text = "3"
        '
        'Value10
        '
        Me.Value10.AutoSize = True
        Me.Value10.Font = New System.Drawing.Font("Comic Sans MS", 19.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Value10.Location = New System.Drawing.Point(1227, 523)
        Me.Value10.Name = "Value10"
        Me.Value10.Size = New System.Drawing.Size(40, 46)
        Me.Value10.TabIndex = 5
        Me.Value10.Text = "6"
        '
        'imgvolume
        '
        Me.imgvolume.Image = CType(resources.GetObject("imgvolume.Image"), System.Drawing.Image)
        Me.imgvolume.Location = New System.Drawing.Point(753, 384)
        Me.imgvolume.Name = "imgvolume"
        Me.imgvolume.Size = New System.Drawing.Size(51, 47)
        Me.imgvolume.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgvolume.TabIndex = 6
        Me.imgvolume.TabStop = False
        '
        'imgQwirkle
        '
        Me.imgQwirkle.Image = CType(resources.GetObject("imgQwirkle.Image"), System.Drawing.Image)
        Me.imgQwirkle.Location = New System.Drawing.Point(633, 149)
        Me.imgQwirkle.Name = "imgQwirkle"
        Me.imgQwirkle.Size = New System.Drawing.Size(654, 173)
        Me.imgQwirkle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgQwirkle.TabIndex = 7
        Me.imgQwirkle.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Comic Sans MS", 19.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(784, 686)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(360, 46)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Affichage de l'écran :"
        '
        'Affichage
        '
        Me.Affichage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Affichage.Font = New System.Drawing.Font("Comic Sans MS", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Affichage.FormattingEnabled = True
        Me.Affichage.Items.AddRange(New Object() {"Plein Ecran", "Fenêtré"})
        Me.Affichage.Location = New System.Drawing.Point(840, 757)
        Me.Affichage.Name = "Affichage"
        Me.Affichage.Size = New System.Drawing.Size(256, 39)
        Me.Affichage.TabIndex = 9
        '
        'AxWindowsMediaPlayer1
        '
        Me.AxWindowsMediaPlayer1.Enabled = True
        Me.AxWindowsMediaPlayer1.Location = New System.Drawing.Point(307, 362)
        Me.AxWindowsMediaPlayer1.Name = "AxWindowsMediaPlayer1"
        Me.AxWindowsMediaPlayer1.OcxState = CType(resources.GetObject("AxWindowsMediaPlayer1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxWindowsMediaPlayer1.Size = New System.Drawing.Size(43, 45)
        Me.AxWindowsMediaPlayer1.TabIndex = 11
        Me.AxWindowsMediaPlayer1.Visible = False
        '
        'ValidParametre
        '
        Me.ValidParametre.Font = New System.Drawing.Font("Comic Sans MS", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ValidParametre.Location = New System.Drawing.Point(840, 883)
        Me.ValidParametre.Name = "ValidParametre"
        Me.ValidParametre.Size = New System.Drawing.Size(256, 55)
        Me.ValidParametre.TabIndex = 12
        Me.ValidParametre.Text = "Valider et Quitter"
        Me.ValidParametre.UseVisualStyleBackColor = True
        '
        'MenuOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1920, 1080)
        Me.Controls.Add(Me.ValidParametre)
        Me.Controls.Add(Me.AxWindowsMediaPlayer1)
        Me.Controls.Add(Me.Affichage)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.imgQwirkle)
        Me.Controls.Add(Me.imgvolume)
        Me.Controls.Add(Me.Value10)
        Me.Controls.Add(Me.Value5)
        Me.Controls.Add(Me.value0)
        Me.Controls.Add(Me.LabelVolume)
        Me.Controls.Add(Me.Volume)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MenuOptions"
        Me.Text = "Options"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.Volume, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgvolume, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgQwirkle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Volume As TrackBar
    Friend WithEvents LabelVolume As Label
    Friend WithEvents value0 As Label
    Friend WithEvents Value5 As Label
    Friend WithEvents Value10 As Label
    Friend WithEvents imgvolume As PictureBox
    Friend WithEvents imgQwirkle As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Affichage As ComboBox
    Friend WithEvents AxWindowsMediaPlayer1 As AxWMPLib.AxWindowsMediaPlayer
    Friend WithEvents ValidParametre As Button
End Class
