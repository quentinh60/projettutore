﻿Imports System.Media
Imports QwirkleLib
Public Class Accueil

    Private Sub Accueil_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PnlNbJ.BackColor = Color.LightGray
    End Sub

    Private Sub PlaySoundResource(sender As Object, e As EventArgs) Handles Quitter.MouseHover, Règles.MouseHover, Jouer.MouseHover, Options.MouseHover, J2.MouseHover, J3.MouseHover, J4.MouseHover

        Dim sndPlayer As New SoundPlayer(My.Resources.Sounds.cmdSound)
        sndPlayer.Play()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Quitter.Click
        Me.Close()
    End Sub

    Private Sub Options_Click(sender As Object, e As EventArgs) Handles Options.Click
        MenuOptions.Show()
        Me.Hide()
    End Sub

    Private Sub Jouez_Click_1(sender As Object, e As EventArgs) Handles Jouer.Click
        Dim nbjoueurs As Byte
        If (J2.Checked) Then
            nbjoueurs = 2
        Else
            If (J3.Checked) Then
                nbjoueurs = 3
            Else
                nbjoueurs = 4
            End If
        End If
        For i As Byte = 1 To nbjoueurs
            Dim newlbl As New Label

            Dim dernierLblCree As New Label

            newlbl.Name = "lbl" & i
            newlbl.Text = "Points Joueur " & i & ":"
            newlbl.Size = New Size(180, 30)
            newlbl.Font = New System.Drawing.Font("Comic Sans MS", 15.0!, System.Drawing.FontStyle.Regular)
            If i = 1 Then
                newlbl.Location = New Point(10, 10)
            Else
                dernierLblCree = PlateauJeu.Controls("lbl" & (i - 1))
                newlbl.Location = New Point(10, dernierLblCree.Location.Y + dernierLblCree.Height + 10)
            End If
            PlateauJeu.Controls.Add(newlbl)

        Next

        Dim tours As New Boolean
        For jo As Integer = 1 To nbjoueurs
            If jo = 1 Then
                tours = True
            End If
            Dim nom As String
            nom = "Joueur" & jo
            Dim J As Joueur = New Joueur(nom, 0, tours, jo, 0)
            PlateauJeu.lesJoueurs.Add(J)
            tours = False
        Next


        PlateauJeu.Show()
        Me.Hide()
    End Sub

    Private Sub Règles_Click(sender As Object, e As EventArgs) Handles Règles.Click
        Process.Start("https://www.iello.fr/regles/Qwirkle_regles_FR_web.pdf")
    End Sub
End Class