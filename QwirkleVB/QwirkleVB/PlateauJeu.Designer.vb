﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PlateauJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PlateauJeu))
        Me.BoutonValiderMouv = New System.Windows.Forms.Button()
        Me.BoutonRefaire = New System.Windows.Forms.Button()
        Me.BoutonQuitter = New System.Windows.Forms.Button()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.picmain1 = New System.Windows.Forms.PictureBox()
        Me.picmain2 = New System.Windows.Forms.PictureBox()
        Me.picmain3 = New System.Windows.Forms.PictureBox()
        Me.picmain4 = New System.Windows.Forms.PictureBox()
        Me.picmain5 = New System.Windows.Forms.PictureBox()
        Me.picmain6 = New System.Windows.Forms.PictureBox()
        Me.picechange2 = New System.Windows.Forms.PictureBox()
        Me.LblEchange = New System.Windows.Forms.Label()
        Me.TimerTour = New System.Windows.Forms.Timer(Me.components)
        Me.picechange3 = New System.Windows.Forms.PictureBox()
        Me.picechange4 = New System.Windows.Forms.PictureBox()
        Me.picechange5 = New System.Windows.Forms.PictureBox()
        Me.picechange6 = New System.Windows.Forms.PictureBox()
        Me.picechange1 = New System.Windows.Forms.PictureBox()
        Me.PBTour = New System.Windows.Forms.ProgressBar()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.LblTps = New System.Windows.Forms.Label()
        Me.PnlEchange = New System.Windows.Forms.Panel()
        CType(Me.picmain1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picmain2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picmain3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picmain4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picmain5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picmain6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picechange2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picechange3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picechange4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picechange5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picechange6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picechange1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PnlEchange.SuspendLayout()
        Me.SuspendLayout()
        '
        'BoutonValiderMouv
        '
        Me.BoutonValiderMouv.Font = New System.Drawing.Font("Comic Sans MS", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BoutonValiderMouv.Location = New System.Drawing.Point(40, 357)
        Me.BoutonValiderMouv.Name = "BoutonValiderMouv"
        Me.BoutonValiderMouv.Size = New System.Drawing.Size(180, 49)
        Me.BoutonValiderMouv.TabIndex = 5
        Me.BoutonValiderMouv.Text = "Valider mouvement"
        Me.BoutonValiderMouv.UseVisualStyleBackColor = True
        '
        'BoutonRefaire
        '
        Me.BoutonRefaire.Font = New System.Drawing.Font("Comic Sans MS", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BoutonRefaire.Location = New System.Drawing.Point(40, 413)
        Me.BoutonRefaire.Name = "BoutonRefaire"
        Me.BoutonRefaire.Size = New System.Drawing.Size(180, 49)
        Me.BoutonRefaire.TabIndex = 6
        Me.BoutonRefaire.Text = "Refaire mouvement"
        Me.BoutonRefaire.UseVisualStyleBackColor = True
        '
        'BoutonQuitter
        '
        Me.BoutonQuitter.Font = New System.Drawing.Font("Comic Sans MS", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BoutonQuitter.Location = New System.Drawing.Point(40, 468)
        Me.BoutonQuitter.Name = "BoutonQuitter"
        Me.BoutonQuitter.Size = New System.Drawing.Size(180, 49)
        Me.BoutonQuitter.TabIndex = 7
        Me.BoutonQuitter.Text = "Quitter la partie"
        Me.BoutonQuitter.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.OrangeRed
        Me.ImageList1.Images.SetKeyName(0, "CarreBleu.jpg")
        Me.ImageList1.Images.SetKeyName(1, "CarreJaune.jpg")
        Me.ImageList1.Images.SetKeyName(2, "CarreOrange.jpg")
        Me.ImageList1.Images.SetKeyName(3, "CarreRouge.jpg")
        Me.ImageList1.Images.SetKeyName(4, "CarreVert.jpg")
        Me.ImageList1.Images.SetKeyName(5, "CarreViolet.jpg")
        Me.ImageList1.Images.SetKeyName(6, "CroixBleue.jpg")
        Me.ImageList1.Images.SetKeyName(7, "CroixJaune.jpg")
        Me.ImageList1.Images.SetKeyName(8, "CroixOrange.jpg")
        Me.ImageList1.Images.SetKeyName(9, "CroixRouge.jpg")
        Me.ImageList1.Images.SetKeyName(10, "CroixVerte.jpg")
        Me.ImageList1.Images.SetKeyName(11, "CroixViolet.jpg")
        Me.ImageList1.Images.SetKeyName(12, "EtoileBleue.jpg")
        Me.ImageList1.Images.SetKeyName(13, "EtoileJaune.jpg")
        Me.ImageList1.Images.SetKeyName(14, "EtoileOrange.jpg")
        Me.ImageList1.Images.SetKeyName(15, "EtoileRouge.jpg")
        Me.ImageList1.Images.SetKeyName(16, "EtoileVerte.jpg")
        Me.ImageList1.Images.SetKeyName(17, "EtoileViolet.jpg")
        Me.ImageList1.Images.SetKeyName(18, "LosangeBleu.jpg")
        Me.ImageList1.Images.SetKeyName(19, "LosangeJaune.jpg")
        Me.ImageList1.Images.SetKeyName(20, "LosangeOrange.jpg")
        Me.ImageList1.Images.SetKeyName(21, "LosangeRouge.jpg")
        Me.ImageList1.Images.SetKeyName(22, "LosangeVert.jpg")
        Me.ImageList1.Images.SetKeyName(23, "LosangeViolet.jpg")
        Me.ImageList1.Images.SetKeyName(24, "RondBleu.jpg")
        Me.ImageList1.Images.SetKeyName(25, "RondJaune.jpg")
        Me.ImageList1.Images.SetKeyName(26, "RondOrange.jpg")
        Me.ImageList1.Images.SetKeyName(27, "RondRouge.jpg")
        Me.ImageList1.Images.SetKeyName(28, "RondVert.jpg")
        Me.ImageList1.Images.SetKeyName(29, "RondViolet.jpg")
        Me.ImageList1.Images.SetKeyName(30, "TrefleBleu.jpg")
        Me.ImageList1.Images.SetKeyName(31, "TrefleJaune.jpg")
        Me.ImageList1.Images.SetKeyName(32, "TrefleOrange.jpg")
        Me.ImageList1.Images.SetKeyName(33, "TrefleRouge.jpg")
        Me.ImageList1.Images.SetKeyName(34, "TrefleVert.jpg")
        Me.ImageList1.Images.SetKeyName(35, "TrefleViolet.jpg")
        '
        'picmain1
        '
        Me.picmain1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picmain1.Location = New System.Drawing.Point(998, 335)
        Me.picmain1.Name = "picmain1"
        Me.picmain1.Size = New System.Drawing.Size(50, 52)
        Me.picmain1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picmain1.TabIndex = 8
        Me.picmain1.TabStop = False
        '
        'picmain2
        '
        Me.picmain2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picmain2.Location = New System.Drawing.Point(1058, 335)
        Me.picmain2.Name = "picmain2"
        Me.picmain2.Size = New System.Drawing.Size(53, 52)
        Me.picmain2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picmain2.TabIndex = 9
        Me.picmain2.TabStop = False
        '
        'picmain3
        '
        Me.picmain3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picmain3.Location = New System.Drawing.Point(1123, 335)
        Me.picmain3.Name = "picmain3"
        Me.picmain3.Size = New System.Drawing.Size(54, 52)
        Me.picmain3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picmain3.TabIndex = 10
        Me.picmain3.TabStop = False
        '
        'picmain4
        '
        Me.picmain4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picmain4.Location = New System.Drawing.Point(1186, 335)
        Me.picmain4.Name = "picmain4"
        Me.picmain4.Size = New System.Drawing.Size(54, 52)
        Me.picmain4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picmain4.TabIndex = 11
        Me.picmain4.TabStop = False
        '
        'picmain5
        '
        Me.picmain5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picmain5.Location = New System.Drawing.Point(1249, 335)
        Me.picmain5.Name = "picmain5"
        Me.picmain5.Size = New System.Drawing.Size(52, 52)
        Me.picmain5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picmain5.TabIndex = 12
        Me.picmain5.TabStop = False
        '
        'picmain6
        '
        Me.picmain6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picmain6.Location = New System.Drawing.Point(1309, 335)
        Me.picmain6.Name = "picmain6"
        Me.picmain6.Size = New System.Drawing.Size(53, 50)
        Me.picmain6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picmain6.TabIndex = 13
        Me.picmain6.TabStop = False
        '
        'picechange2
        '
        Me.picechange2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picechange2.Location = New System.Drawing.Point(77, 18)
        Me.picechange2.Name = "picechange2"
        Me.picechange2.Size = New System.Drawing.Size(45, 50)
        Me.picechange2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picechange2.TabIndex = 14
        Me.picechange2.TabStop = False
        '
        'LblEchange
        '
        Me.LblEchange.AutoSize = True
        Me.LblEchange.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEchange.Location = New System.Drawing.Point(1104, 469)
        Me.LblEchange.Name = "LblEchange"
        Me.LblEchange.Size = New System.Drawing.Size(135, 23)
        Me.LblEchange.TabIndex = 16
        Me.LblEchange.Text = "Echange de tuiles"
        '
        'TimerTour
        '
        Me.TimerTour.Enabled = True
        Me.TimerTour.Interval = 1000
        '
        'picechange3
        '
        Me.picechange3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picechange3.Location = New System.Drawing.Point(128, 18)
        Me.picechange3.Name = "picechange3"
        Me.picechange3.Size = New System.Drawing.Size(45, 50)
        Me.picechange3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picechange3.TabIndex = 568
        Me.picechange3.TabStop = False
        '
        'picechange4
        '
        Me.picechange4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picechange4.Location = New System.Drawing.Point(179, 18)
        Me.picechange4.Name = "picechange4"
        Me.picechange4.Size = New System.Drawing.Size(45, 50)
        Me.picechange4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picechange4.TabIndex = 569
        Me.picechange4.TabStop = False
        '
        'picechange5
        '
        Me.picechange5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picechange5.Location = New System.Drawing.Point(230, 18)
        Me.picechange5.Name = "picechange5"
        Me.picechange5.Size = New System.Drawing.Size(45, 50)
        Me.picechange5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picechange5.TabIndex = 570
        Me.picechange5.TabStop = False
        '
        'picechange6
        '
        Me.picechange6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picechange6.Location = New System.Drawing.Point(281, 18)
        Me.picechange6.Name = "picechange6"
        Me.picechange6.Size = New System.Drawing.Size(45, 50)
        Me.picechange6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picechange6.TabIndex = 571
        Me.picechange6.TabStop = False
        '
        'picechange1
        '
        Me.picechange1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picechange1.Location = New System.Drawing.Point(26, 18)
        Me.picechange1.Name = "picechange1"
        Me.picechange1.Size = New System.Drawing.Size(45, 50)
        Me.picechange1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picechange1.TabIndex = 572
        Me.picechange1.TabStop = False
        '
        'PBTour
        '
        Me.ErrorProvider1.SetIconAlignment(Me.PBTour, System.Windows.Forms.ErrorIconAlignment.MiddleLeft)
        Me.PBTour.Location = New System.Drawing.Point(501, 37)
        Me.PBTour.Margin = New System.Windows.Forms.Padding(2)
        Me.PBTour.MarqueeAnimationSpeed = 1000
        Me.PBTour.Maximum = 60
        Me.PBTour.Name = "PBTour"
        Me.PBTour.Size = New System.Drawing.Size(404, 16)
        Me.PBTour.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.PBTour.TabIndex = 573
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'LblTps
        '
        Me.LblTps.AutoSize = True
        Me.LblTps.Font = New System.Drawing.Font("Comic Sans MS", 14.0!)
        Me.LblTps.Location = New System.Drawing.Point(632, 7)
        Me.LblTps.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.LblTps.Name = "LblTps"
        Me.LblTps.Size = New System.Drawing.Size(139, 26)
        Me.LblTps.TabIndex = 574
        Me.LblTps.Text = "Temps restant"
        '
        'PnlEchange
        '
        Me.PnlEchange.Controls.Add(Me.picechange6)
        Me.PnlEchange.Controls.Add(Me.picechange5)
        Me.PnlEchange.Controls.Add(Me.picechange4)
        Me.PnlEchange.Controls.Add(Me.picechange1)
        Me.PnlEchange.Controls.Add(Me.picechange3)
        Me.PnlEchange.Controls.Add(Me.picechange2)
        Me.PnlEchange.Location = New System.Drawing.Point(1012, 389)
        Me.PnlEchange.Name = "PnlEchange"
        Me.PnlEchange.Size = New System.Drawing.Size(339, 73)
        Me.PnlEchange.TabIndex = 575
        '
        'PlateauJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(1386, 788)
        Me.Controls.Add(Me.PnlEchange)
        Me.Controls.Add(Me.LblTps)
        Me.Controls.Add(Me.PBTour)
        Me.Controls.Add(Me.LblEchange)
        Me.Controls.Add(Me.picmain6)
        Me.Controls.Add(Me.picmain5)
        Me.Controls.Add(Me.picmain4)
        Me.Controls.Add(Me.picmain3)
        Me.Controls.Add(Me.picmain2)
        Me.Controls.Add(Me.picmain1)
        Me.Controls.Add(Me.BoutonQuitter)
        Me.Controls.Add(Me.BoutonRefaire)
        Me.Controls.Add(Me.BoutonValiderMouv)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "PlateauJeu"
        Me.Text = "Partie de Qwirkle"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.picmain1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picmain2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picmain3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picmain4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picmain5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picmain6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picechange2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picechange3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picechange4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picechange5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picechange6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picechange1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PnlEchange.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BoutonValiderMouv As Button
    Friend WithEvents BoutonRefaire As Button
    Friend WithEvents BoutonQuitter As Button
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents picmain1 As PictureBox
    Friend WithEvents picmain2 As PictureBox
    Friend WithEvents picmain3 As PictureBox
    Friend WithEvents picmain4 As PictureBox
    Friend WithEvents picmain5 As PictureBox
    Friend WithEvents picmain6 As PictureBox
    Friend WithEvents picechange2 As PictureBox
    Friend WithEvents LblEchange As Label
    Friend WithEvents TimerTour As Timer
    Friend WithEvents picechange3 As PictureBox
    Friend WithEvents picechange4 As PictureBox
    Friend WithEvents picechange5 As PictureBox
    Friend WithEvents picechange6 As PictureBox
    Friend WithEvents picechange1 As PictureBox
    Friend WithEvents PBTour As ProgressBar
    Friend WithEvents ErrorProvider1 As ErrorProvider
    Friend WithEvents LblTps As Label
    Friend WithEvents PnlEchange As Panel
End Class
