﻿Option Strict On
Imports System.Runtime.InteropServices
Public Class MenuOptions
    Private Const APPCOMMAND_VOLUME_UP As Integer = &HA0000
    Private Const APPCOMMAND_VOLUME_DOWN As Integer = &H90000
    Private Const WM_APPCOMMAND As Integer = &H319
    Dim TrcBarValue As Integer
    <DllImport("user32.dll")> Public Shared Function SendMessageW(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As IntPtr
    End Function
    Private Sub TrackBar1_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Volume.ValueChanged
        TrcBarValue = Volume.Value
        LabelVolume.Text = "Volume : " & Volume.Value.ToString()
    End Sub
    Private Sub TrackBar1_Scroll(ByVal sender As Object, ByVal e As EventArgs) Handles Volume.Scroll
        Dim TrcBarDirection As Integer = Volume.Value
        If Volume.Value > TrcBarValue Then
            For TrcBarDirection = 0 To TrcBarDirection
                SendMessageW(Me.Handle, WM_APPCOMMAND, Me.Handle, New IntPtr(APPCOMMAND_VOLUME_UP))
            Next
        Else : For TrcBarDirection = 0 To TrcBarValue
                SendMessageW(Me.Handle, WM_APPCOMMAND, Me.Handle, New IntPtr(APPCOMMAND_VOLUME_DOWN))
            Next
        End If
    End Sub

    Private Sub MenuOptions_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Volume.Maximum = 6
        Volume.Value = 1
        Affichage.SelectedIndex = 0
    End Sub

    Private Sub Affichage_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Affichage.SelectedIndexChanged
        If (Affichage.SelectedIndex = 0) Then
            Me.WindowState = FormWindowState.Maximized
            Me.FormBorderStyle = FormBorderStyle.None
            Me.TopMost = True
        Else
            Me.WindowState = FormWindowState.Maximized
            Me.Location = New Point(0, 0)
            Me.FormBorderStyle = FormBorderStyle.FixedToolWindow
        End If

    End Sub

    Private Sub ValidParametre_Click(sender As Object, e As EventArgs) Handles ValidParametre.Click
        If (Affichage.SelectedIndex = 0) Then
            Accueil.WindowState = FormWindowState.Maximized
            Accueil.FormBorderStyle = FormBorderStyle.None
            Me.TopMost = True
            PlateauJeu.WindowState = FormWindowState.Maximized
            PlateauJeu.FormBorderStyle = FormBorderStyle.None
        Else
            Accueil.WindowState = FormWindowState.Maximized
            Accueil.Location = New Point(0, 0)
            Accueil.FormBorderStyle = FormBorderStyle.FixedToolWindow
            Accueil.Height = Me.Height
            PlateauJeu.WindowState = FormWindowState.Maximized
            Accueil.Location = New Point(0, 0)
            PlateauJeu.FormBorderStyle = FormBorderStyle.FixedToolWindow
        End If
        Accueil.Show()
        Me.Hide()
    End Sub
End Class