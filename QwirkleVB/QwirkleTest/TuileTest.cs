﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleLib;

namespace QwirkleTest
{
    [TestClass]
    public class TuileTest
    {
        [TestMethod]
        public void TestTuile()
        {
            Tuile Tuile = new Tuile("Bleu", "Etoile");
            Assert.AreEqual("Bleu", Tuile.getColor());
            Assert.AreEqual("Etoile", Tuile.getSymbol());
        }
    }
}
