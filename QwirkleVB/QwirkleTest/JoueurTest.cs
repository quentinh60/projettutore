﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QwirkleLib;

namespace QwirkleTest
{
    [TestClass]
    public class JoueurTest
    {
        [TestMethod]
        public void TestJoueur()
        {
            Joueur Joueur1 = new Joueur("Joueur1",0,false,1,0);
            Assert.AreEqual("Joueur1", Joueur1.GetNom());
            Assert.AreEqual(0,Joueur1.GetScore());
            Assert.AreEqual(false,Joueur1.GetTour());
            Assert.AreEqual(1,Joueur1.GetNoJoueur());
        }

        [TestMethod]
        public void TestJoueurSetScore()
        {
            Joueur Joueur1 = new Joueur("Joueur1", 0, false, 1, 0);
            Joueur1.SetScore(180);
            Assert.AreEqual(180, Joueur1.GetScore());
        }

        [TestMethod]
        public void TestJoueurSetTour()
        {
            Joueur Joueur1 = new Joueur("Joueur1", 0, false, 1, 0);
            Joueur1.SetTour(true);
            Assert.AreEqual(true, Joueur1.GetTour());
        }
        
    }
}
